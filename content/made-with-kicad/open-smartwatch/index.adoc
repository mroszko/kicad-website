+++
title = "Open Smartwatch"
projectdeveloper = "pauls_3d_things"
projecturl = "https://github.com/Open-Smartwatch/kicad-project"
"made-with-kicad/categories" = [
    "Single-board Computer",
    "Wireless"
]
+++


link:https://open-smartwatch.github.io/[Open Smartwatch] is a completely open (ECAD/MCAD and software) smart watch. The goal is to build an open source smartwatch, with gps tracking and maps. Features include: ESP32, GPS, Time, MEMS sensors, Li-ion battery, USB serial and microSD card storage.

