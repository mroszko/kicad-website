+++
title = "kicad.org, the permanent internet home of KiCad"
date = "2020-10-29"
draft = false
"blog/categories" = [
    "News"
]
+++

:icons:
:iconsdir: /img/icons/

Thanks to a generous donation by https://www.digikey.com[Digi-Key], the KiCad project
is now found at the https://kicad.org[kicad.org] domain.  

The kicad.org domain provides a stable, clear home for the KiCad project.  We are
extremely grateful to Digi-Key for their support of KiCad.

IMPORTANT: kicad-pcb.org is now under control of an unknown entity the project has no relationship with. All previous links are unforunately broken.